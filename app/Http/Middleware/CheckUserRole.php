<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckUserRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
  public function handle(Request $request, Closure $next, $role)
  {
    // Получаем текущего пользователя из запроса
    $user = $request->user();

    // Проверяем, что пользователь существует и его роль соответствует заданной
    if ($user && $user->role == $role) {
      // Пропускаем запрос дальше
      return $next($request);
    }

    // Возвращаем ответ с кодом 403 (доступ запрещен)
    return response()->json(['message' => 'You are not authorized to access this resource'], 403);
  }
}
