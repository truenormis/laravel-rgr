<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class UserController extends Controller
{
  public function index()
  {
    return view('content.pages.user.list');
  }

  public function list()
  {
    // Получаем всех пользователей из базы данных
    $users = User::all();


    // Формируем массив данных для ответа
    $data = [];
    foreach ($users as $user) {
      $role = ($user->role == "admin") ? 3 : (($user->role == "driver") ? 2 : 1);
      $birthday = Carbon::parse($user->date_of_birth);
      $age = $birthday->age;

      $data[] = [
        'id' => $user->id,
        //'avatar' => '1.png',
        'username' => $user->name,
        'full_name' => $user->full_name,
        'email' => $user->email,
        'age' => $age,
        'status' => $role
      ];
    }

    // Возвращаем ответ в JSON формате с кодом 200
    return response()->json(['data' => $data], 200);
  }

  public function update(Request $request, User $user)
  {
    $validatedData = $request->validate([
      'name' => 'required|string|max:255',
      'full_name' => 'required|string|max:255',
      'date_of_birth' => 'required|date',
      'role' => 'required|in:user,driver,admin',
      'email' => [
        'required',
        'string',
        'email',
        'max:255',
        Rule::unique('users')->ignore($user->id), // Игнорируем текущего пользователя
      ],
      'password' => 'nullable|string|min:8|confirmed',
    ]);

    // Обновляем данные пользователя в базе данных
    $userData = [
      'name' => $validatedData['name'],
      'full_name' => $validatedData['full_name'],
      'date_of_birth' => $validatedData['date_of_birth'],
      'role' => $validatedData['role'],
      'email' => $validatedData['email'],
    ];

    // Проверяем, был ли предоставлен новый пароль
    if ($request->filled('password')) {
      $userData['password'] = Hash::make($validatedData['password']);
    }

    $user->update($userData);

    return response()->json(['message' => 'User updated successfully'], 200);
  }


  public function destroy(User $user)
  {
    // Удаляем пользователя из базы данных
    $user->delete();
    return response()->json(['message' => 'Пользователь успешно удален'], ResponseAlias::HTTP_OK);

  }

  public function info(User $user)
  {


    $data[] = [
      'id' => $user->id,
      'name' => $user->name,
      'full_name' => $user->full_name,
      'email' => $user->email,
      'date_of_birth' => $user->date_of_birth,
      'role' => $user->role,
    ];


    // Возвращаем ответ в JSON формате с кодом 200
    return response()->json(['data' => $data], 200);

  }

  public function store(Request $request)
  {
    $validatedData = $request->validate([
      'name' => 'required|string|max:255|regex:/^[a-zA-Z0-9]+$/',
      'full_name' => 'required|string|max:255|regex:/^[a-zA-Z0-9]+$/',
      'email' => 'required|string|email|max:255|unique:users',
      'date_of_birth' => 'required|date',
      'role' => 'required|in:user,driver,admin|regex:/^[a-zA-Z0-9]+$/',
      'password' => 'required|string|min:8|confirmed',
    ]);

    // Если валидация успешно пройдена, создаем нового пользователя в базе данных
    $user = User::create([
      'name' => $request->name,
      'full_name' => $request->full_name,
      'email' => $request->email,
      'date_of_birth' => $request->date_of_birth,
      'role' => $request->role,
      'password' => Hash::make($validatedData['password']),
    ]);


    // Возвращаем успешный HTTP-код (например, 200)
    return response()->json(['message' => 'User created successfully', 'user' => $user, 'rec' => $request->all()], 200);
  }
}
