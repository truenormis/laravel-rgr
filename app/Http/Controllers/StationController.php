<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreStationRequest;
use App\Http\Requests\UpdateStationRequest;
use App\Models\Station;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class StationController extends Controller
{
  public function index()
  {
    return view('content.pages.station.list');
  }

  public function list()
  {
    // Получаем всех пользователей из базы данных
    $stations = Station::all();


    // Формируем массив данных для ответа
    $data = [];
    foreach ($stations as $station) {

      $data[] = [
        'id' => $station->id,
        'name' => $station->name,
      ];
    }

    // Возвращаем ответ в JSON формате с кодом 200
    return response()->json(['data' => $data], 200);
  }

  public function update(Request $request, Station $station)
  {
    $validatedData = $request->validate([
      'name' => 'required|string|max:255',
    ]);

    // Обновляем данные пользователя в базе данных
    $StationData = [
      'name' => $validatedData['name'],
    ];


    $station->update($StationData);

    return response()->json(['message' => 'User updated successfully'], 200);
  }


  public function destroy(Station $station)
  {

    $station->delete();
    return response()->json(['message' => 'Station Destroyed'], ResponseAlias::HTTP_OK);

  }

  public function info(Station $station)
  {
    $data[] = [
      'id' => $station->id,
      'name' => $station->name,
    ];


    // Возвращаем ответ в JSON формате с кодом 200
    return response()->json(['data' => $data], 200);

  }

  public function store(Request $request)
  {
    $validatedData = $request->validate([
      'name' => 'required|string|max:255',
    ]);

    // Если валидация успешно пройдена, создаем нового пользователя в базе данных
    $station = Station::create([
      'name' => $request->name,
    ]);


    // Возвращаем успешный HTTP-код (например, 200)
    return response()->json(['message' => 'Station created successfully', 'station' => $station, 'rec' => $request->all()], 200);
  }
}
