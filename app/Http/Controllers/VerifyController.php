<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;

class VerifyController extends Controller
{
  public function checkEmail(Request $request)
  {
    $email = $request->input('email');

    // Проверяем, существует ли email в базе данных
    $user = User::where('email', $email)->first();

    if ($user) {
      // Email существует, отправляем сообщение об ошибке
      return response()->json(['valid' => 'false']);
    }

    // Email уникален
    return response()->json(['valid' => 'true']);
  }
}
