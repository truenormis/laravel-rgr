<?php

namespace App\Http\Controllers;

use App\Models\Train;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TrainController extends Controller
{
  public function index()
  {
    return view('content.pages.train.list');
  }

  public function list()
  {
    $trains = Train::all();

    $data = [];
    foreach ($trains as $train) {
      $data[] = [
        'id' => $train->id,
        'number' => $train->number,
        'model' => $train->model,
        'user_id' => $train->user_id,
        'created_at' => $train->created_at,
        'updated_at' => $train->updated_at,
      ];
    }

    return response()->json(['data' => $data], 200);
  }

  public function update(Request $request, Train $train)
  {
    $validatedData = $request->validate([
      'number' => [
        'required',
        'string',
        'max:255',
        Rule::unique('trains')->ignore($train->id),
      ],
      'model' => 'required|string|max:255',
      'user_id' => 'nullable|exists:users,id',
    ]);

    $train->update($validatedData);

    return response()->json(['message' => 'Train updated successfully'], 200);
  }

  public function destroy(Train $train)
  {
    $train->delete();

    return response()->json(['message' => 'Train successfully deleted'], ResponseAlias::HTTP_OK);
  }

  public function info(Train $train)
  {
    return response()->json(['data' => $train], 200);
  }

  public function store(Request $request)
  {
    $validatedData = $request->validate([
      'number' => 'required|string|max:255|unique:trains',
      'model' => 'required|string|max:255',
      'user_id' => 'nullable|exists:users,id',
    ]);

    $train = Train::create($validatedData);

    return response()->json(['message' => 'Train created successfully', 'train' => $train], 200);
  }
}
