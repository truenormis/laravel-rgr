<?php

namespace App\Http\Controllers\authentications;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisterBasic extends Controller
{
  public function index()
  {
    $pageConfigs = ['myLayout' => 'blank'];
    return view('content.authentications.auth-register-basic', ['pageConfigs' => $pageConfigs]);
  }

  public function store(Request $request)
  {
    //dd($request->all());
    $user = User::create([
      'name' => $request->username,
      'email' => $request->email,
      'password' => Hash::make($request->password)
    ]);

    Auth::login($user);
    return redirect(RouteServiceProvider::HOME);
  }
}
