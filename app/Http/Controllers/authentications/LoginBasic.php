<?php

namespace App\Http\Controllers\authentications;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginBasic extends Controller
{
  public function index()
  {
    $pageConfigs = ['myLayout' => 'blank'];
    return view('content.authentications.auth-login-basic', ['pageConfigs' => $pageConfigs]);
  }

  public function store(Request $request)
  {
    $usernameOrEmail = $request->input('email-username');
    $password = $request->input('password');
    $remember = $request->has('remember'); // Проверяем, установлена ли галочка Remember Me

    if (filter_var($usernameOrEmail, FILTER_VALIDATE_EMAIL)) {
      // Если введен email
      $credentials = ['email' => $usernameOrEmail, 'password' => $password];
    } else {
      // Если введено имя пользователя
      $credentials = ['name' => $usernameOrEmail, 'password' => $password];
    }

    if (Auth::attempt($credentials, $remember)) {
      // Аутентификация прошла успешно
      return redirect()->intended('/dashboard'); // Перенаправление на страницу после входа
    }


    // Аутентификация не удалась
    return back()->withErrors(['email-username' => 'Неверный email или пароль']);
  }
}
