<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



// Main Page Route
Route::get('/', function (){return redirect(\App\Providers\RouteServiceProvider::HOME);});
Route::get('/dashboard', [\App\Http\Controllers\pages\HomePage::class,'index'])->middleware('auth')->name('home');
//Route::get('/page-2', 'Page2@index')->name('pages-page-2');

// pages
//Route::get('/pages/misc-error', 'MiscError@index')->name('pages-misc-error');

// authentication
Route::get('/auth/login', [\App\Http\Controllers\authentications\LoginBasic::class,'index'])->middleware('guest')->name('login.index');
Route::post('/auth/login', [\App\Http\Controllers\authentications\LoginBasic::class,'store'])->middleware('guest')->name('login.store');
Route::get('/auth/register', [\App\Http\Controllers\authentications\RegisterBasic::class,'index'])->middleware('guest')->name('register.index');
Route::post('/auth/register', [\App\Http\Controllers\authentications\RegisterBasic::class,'store'])->middleware('guest')->name('register.store');


Route::post('/check-email', [\App\Http\Controllers\VerifyController::class,'checkEmail']);


Route::group(['middleware' => ['auth', 'role:admin'], 'prefix' => 'users', 'as' => 'users.'], function () {
  Route::get('/', [\App\Http\Controllers\UserController::class, 'index'])->name('index');
  Route::get('/list', [\App\Http\Controllers\UserController::class, 'list'])->name('list');
  Route::get('/{user}', [\App\Http\Controllers\UserController::class, 'info'])->name('info');
  Route::put('/{user}', [\App\Http\Controllers\UserController::class, 'update'])->name('update');
  Route::delete('/{user}', [\App\Http\Controllers\UserController::class, 'destroy'])->name('destroy');
  Route::post('/store', [\App\Http\Controllers\UserController::class, 'store'])->name('store');
});

Route::group(['middleware' => ['auth', 'role:admin'], 'prefix' => 'trains', 'as' => 'trains.'], function () {
  Route::get('/', [\App\Http\Controllers\TrainController::class, 'index'])->name('index');
  Route::get('/list', [\App\Http\Controllers\TrainController::class, 'list'])->name('list');
  Route::get('/{train}', [\App\Http\Controllers\TrainController::class, 'info'])->name('info');
  Route::put('/{train}', [\App\Http\Controllers\TrainController::class, 'update'])->name('update');
  Route::delete('/{train}', [\App\Http\Controllers\TrainController::class, 'destroy'])->name('destroy');
  Route::post('/store', [\App\Http\Controllers\TrainController::class, 'store'])->name('store');
});


Route::group(['middleware' => ['auth', 'role:admin'], 'prefix' => 'station', 'as' => 'station.'], function () {
  Route::get('/', [\App\Http\Controllers\StationController::class, 'index'])->name('index');
  Route::get('/list', [\App\Http\Controllers\StationController::class, 'list'])->name('list');
  Route::get('/{station}', [\App\Http\Controllers\StationController::class, 'info'])->name('info');
  Route::put('/{station}', [\App\Http\Controllers\StationController::class, 'update'])->name('update');
  Route::delete('/{station}', [\App\Http\Controllers\StationController::class, 'destroy'])->name('destroy');
  Route::post('/store', [\App\Http\Controllers\StationController::class, 'store'])->name('store');
});
