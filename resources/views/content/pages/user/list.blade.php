@extends('layouts/layoutMaster')

@section('title', 'User List - Pages')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('assets/vendor/libs/datatables-bs5/datatables.bootstrap5.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/libs/datatables-responsive-bs5/responsive.bootstrap5.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/libs/datatables-buttons-bs5/buttons.bootstrap5.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/libs/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/formvalidation/dist/css/formValidation.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/animate-css/animate.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/sweetalert2/sweetalert2.css')}}" />

@endsection

@section('vendor-script')
<script src="{{asset('assets/vendor/libs/moment/moment.js')}}"></script>
<script src="{{asset('assets/vendor/libs/datatables-bs5/datatables-bootstrap5.js')}}"></script>
<script src="{{asset('assets/vendor/libs/select2/select2.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/FormValidation.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/plugins/Bootstrap5.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/plugins/AutoFocus.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/cleavejs/cleave.js')}}"></script>
<script src="{{asset('assets/vendor/libs/cleavejs/cleave-phone.js')}}"></script>
<script src="{{asset('assets/vendor/libs/sweetalert2/sweetalert2.js')}}"></script>
@endsection

@section('page-script')
  <script>
    $(function() {
      'use strict';

      var dt_basic_table = $('.datatables-basic');

      // DataTable with buttons
      // --------------------------------------------------------------------

      if (dt_basic_table.length) {
        dt_basic = dt_basic_table.DataTable({
          ajax: '{{route('users.list')}}',
          columns: [
            { data: '' },
            { data: 'id' },
            { data: 'id' },
            { data: 'full_name' },
            { data: 'email' },
            { data: 'age' },
            { data: 'status' },
            { data: '' }
          ],
          columnDefs: [
            {
              // For Responsive
              className: 'control',
              orderable: false,
              searchable: false,
              responsivePriority: 2,
              targets: 0,
              render: function (data, type, full, meta) {
                return '';
              }
            },
            {
              // For Checkboxes
              targets: 1,
              orderable: false,
              searchable: false,
              responsivePriority: 3,
              checkboxes: true,
              render: function () {
                return '<input type="checkbox" class="dt-checkboxes form-check-input">';
              },
              checkboxes: {
                selectAllRender: '<input type="checkbox" class="form-check-input">'
              }
            },
            {
              targets: 2,
              searchable: false,
              visible: false
            },
            {
              // Avatar image/badge, Name and post
              targets: 3,
              responsivePriority: 4,
              render: function (data, type, full, meta) {
                var $user_img = full['avatar'],
                  $name = full['full_name'],
                  $username = full['username'];
                if ($user_img) {
                  // For Avatar image
                  var $output =
                    '<img src="' + assetsPath + 'img/avatars/' + $user_img + '" alt="Avatar" class="rounded-circle">';
                } else {
                  // For Avatar badge
                  var stateNum = Math.floor(Math.random() * 6);
                  var states = ['success', 'danger', 'warning', 'info', 'primary', 'secondary'];
                  var $state = states[stateNum],
                    $name = full['full_name'],
                    $initials = $name.match(/\b\w/g) || [];
                  $initials = (($initials.shift() || '') + ($initials.pop() || '')).toUpperCase();
                  $output = '<span class="avatar-initial rounded-circle bg-label-' + $state + '">' + $initials + '</span>';
                }
                // Creates full output for row
                var $row_output =
                  '<div class="d-flex justify-content-start align-items-center user-name">' +
                  '<div class="avatar-wrapper">' +
                  '<div class="avatar me-2">' +
                  $output +
                  '</div>' +
                  '</div>' +
                  '<div class="d-flex flex-column">' +
                  '<span class="emp_name text-truncate">' +
                  $name +
                  '</span>' +
                  '<small class="emp_post text-truncate text-muted">' +
                  $username +
                  '</small>' +
                  '</div>' +
                  '</div>';
                return $row_output;
              }
            },
            {
              responsivePriority: 1,
              targets: 4
            },
            {
              // Label
              targets: -2,
              render: function (data, type, full, meta) {
                var $status_number = full['status'];
                var $status = {
                  1: { title: 'User', class: 'bg-label-primary' },
                  2: { title: 'Driver', class: ' bg-label-success' },
                  3: { title: 'Admin', class: ' bg-label-danger' },

                };
                if (typeof $status[$status_number] === 'undefined') {
                  return data;
                }
                return (
                  '<span class="badge ' + $status[$status_number].class + '">' + $status[$status_number].title + '</span>'
                );
              }
            },
            {
              // Actions
              targets: -1,
              title: 'Actions',
              orderable: false,
              searchable: false,
              render: function (data, type, full, meta) {
                return (
                  '<div class="d-inline-block">' +
                  '<a href="javascript:;" class="btn btn-sm btn-icon dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="text-primary ti ti-dots-vertical"></i></a>' +
                  '<ul class="dropdown-menu dropdown-menu-end m-0">' +
                  '<li><a href="javascript:;" class="dropdown-item">Details</a></li>' +
                  '<li><a href="javascript:;" class="dropdown-item">Archive</a></li>' +
                  '<div class="dropdown-divider"></div>' +
                  '<li><a href="javascript:;" class="dropdown-item text-danger delete-record" data-id="'+full['id']+'">Delete</a></li>' +
                  '</ul>' +
                  '</div>' +
                  '<a href="javascript:;" class="btn btn-sm btn-icon item-edit" data-id="'+full['id']+'" data-bs-toggle="modal" data-bs-target="#editModal"><i class="text-primary ti ti-pencil"></i></a>'
                );
              }
            }
          ],
          order: [[2, 'desc']],
          dom: '<"card-header flex-column flex-md-row"<"head-label text-center"><"dt-action-buttons text-end pt-3 pt-md-0"B>><"row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6 d-flex justify-content-center justify-content-md-end"f>>t<"row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
          displayLength: 7,
          lengthMenu: [7, 10, 25, 50, 75, 100],
          buttons: [
            {
              extend: 'collection',
              className: 'btn btn-label-primary dropdown-toggle me-2',
              text: '<i class="ti ti-file-export me-sm-1"></i> <span class="d-none d-sm-inline-block">Export</span>',
              buttons: [
                {
                  extend: 'print',
                  text: '<i class="ti ti-printer me-1" ></i>Print',
                  className: 'dropdown-item',
                  exportOptions: { columns: [3, 4, 5, 6, 7] }
                },
                {
                  extend: 'csv',
                  text: '<i class="ti ti-file-text me-1" ></i>Csv',
                  className: 'dropdown-item',
                  exportOptions: { columns: [3, 4, 5, 6, 7] }
                },
                {
                  extend: 'pdf',
                  text: '<i class="ti ti-file-description me-1"></i>Pdf',
                  className: 'dropdown-item',
                  exportOptions: { columns: [3, 4, 5, 6, 7] }
                },
                {
                  extend: 'copy',
                  text: '<i class="ti ti-copy me-1" ></i>Copy',
                  className: 'dropdown-item',
                  exportOptions: { columns: [3, 4, 5, 6, 7] }
                }
              ]
            },
            {
              text: '<i class="ti ti-plus me-sm-1"></i> <span class="d-none d-sm-inline-block" data-bs-toggle="modal" data-bs-target="#createModal">Add New Record</span>',
              className: 'create-new btn btn-primary'
            }
          ],
          responsive: {
            details: {
              display: $.fn.dataTable.Responsive.display.modal({
                header: function (row) {
                  var data = row.data();
                  return 'Details of ' + data['full_name'];
                }
              }),
              type: 'column',
              renderer: function (api, rowIdx, columns) {
                var data = $.map(columns, function (col, i) {
                  return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                    ? '<tr data-dt-row="' +
                    col.rowIndex +
                    '" data-dt-column="' +
                    col.columnIndex +
                    '">' +
                    '<td>' +
                    col.title +
                    ':' +
                    '</td> ' +
                    '<td>' +
                    col.data +
                    '</td>' +
                    '</tr>'
                    : '';
                }).join('');

                return data ? $('<table class="table"/><tbody />').append(data) : false;
              }
            }
          }
        });
        $('div.head-label').html('<h5 class="card-title mb-0">DataTable with Buttons</h5>');
      }
    });
    $(document).on('click', '.delete-record', function () {
      var user_id = $(this).data('id');


      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        customClass: {
          confirmButton: 'btn btn-primary me-3',
          cancelButton: 'btn btn-label-secondary'
        },
        buttonsStyling: false
      }).then((result) => {
        if (result.isConfirmed) {
          console.log(user_id);
          $.ajax({
            url: '/users/' + user_id,
            type: 'DELETE', // Метод запроса
            data: { // Данные, которые отправляются на сервер
              _token: $('meta[name="csrf-token"]').attr('content') // Токен для защиты от CSRF атак
            },
            success: function (response) { // Функция, которая вызывается при успешном ответе от сервера
              // После успешного удаления, выведите сообщение
              Swal.fire({
                title: 'Deleted!',
                text: 'The record has been deleted.',
                icon: 'success',
                customClass: {
                  confirmButton: 'btn btn-primary'
                },
                buttonsStyling: false
              });
              // Обновите таблицу с данными пользователей
              $('.datatables-basic').DataTable().ajax.reload();
            },
            error: function (error) { // Функция, которая вызывается при ошибке на сервере
              // Выведите сообщение об ошибке
              Swal.fire({
                title: 'Error!',
                text: error.responseJSON.message,
                icon: 'error',
                customClass: {
                  confirmButton: 'btn btn-primary'
                },
                buttonsStyling: false
              });
            }
          });
        }
      });
    });
    $('#createModal .btn-primary').on('click', function() {
      // Получаем данные из формы
      var username = $('#Username').val();
      var full_name = $('#fullname').val();
      var email = $('#emailBasic').val();
      var dob = $('#dobBasic').val();
      var role = $('#select2Basic').val();
      var password = $('#password').val();
      var password_confirm = $('#password_conft').val();

      // Проверяем, что все поля заполнены
      if (username && full_name && email && dob && role && password && password_confirm) {
        // Отправляем ajax-запрос на сервер
        $.ajax({
          url: '/users/store', // URL для создания пользователя
          type: 'POST', // Метод запроса
          data: { // Данные, которые отправляем
            name: username,
            full_name: full_name,
            email: email,
            date_of_birth: dob,
            role: role,
            password: password,
            password_confirmation: password_confirm,
            _token: $('meta[name="csrf-token"]').attr('content')
          },
          success: function(response) { // Функция, которая вызывается при успешном ответе
            // Закрываем модальное окно
            $('#createModal').modal('hide');
            // Очищаем форму
            $('#createModal input').val('');
            $('#select2Basic').val('user');
            // Выводим сообщение об успехе с Sweet Alert
            Swal.fire({
              icon: 'success',
              title: 'Успех!',
              text: response.message,
            });
            $('.datatables-basic').DataTable().ajax.reload();
          },
          error: function(error) { // Функция, которая вызывается при ошибке
            // Выводим сообщение об ошибке с Sweet Alert
            Swal.fire({
              icon: 'error',
              title: 'Ошибка!',
              text: error.responseJSON.message,
            });
          }
        });
      } else {
        // Выводим сообщение о том, что нужно заполнить все поля с Sweet Alert
        Swal.fire({
          icon: 'error',
          title: 'Ошибка!',
          text: 'Пожалуйста, заполните все поля',
        });
      }
    });



    $(document).on('click', '.item-edit', function () {
      // Получаем id пользователя из атрибута data-id
      var user_id = $(this).data('id');
      // Отправляем ajax-запрос на сервер для получения данных пользователя
      $.ajax({
        url: '/users/' + user_id, // URL для получения информации о пользователе
        type: 'GET', // Метод запроса
        success: function(response) { // Функция, которая вызывается при успешном ответе
          // Получаем данные пользователя из ответа
          var user = response.data[0];
          // Заполняем форму редактирования данными пользователя
          $('#editModal #Username').val(user.name);
          $('#editModal #fullname').val(user.full_name);
          $('#editModal #emailBasic').val(user.email);
          $('#editModal #dobBasic').val(user.date_of_birth);
          $('#editModal #select2Basic').val(user.role);
          $('#editModal .btn-primary').data('id',user_id)

        },
        error: function(error) { // Функция, которая вызывается при ошибке
          // Выводим сообщение об ошибке
          alert(error.responseJSON.message);
        }
      });
    });


    $('#editModal .btn-primary').on('click', function() {
      var user_id = $(this).data('id');
      // Получаем данные из формы
      var username = $('#editModal #Username').val();
      var full_name = $('#editModal #fullname').val();
      var email = $('#editModal #emailBasic').val();
      var dob = $('#editModal #dobBasic').val();
      var role = $('#editModal #select2Basic').val();
      var password = $('#editModal #password').val();
      var password_confirm = $('#editModal #password_conft').val();

      // Проверяем, что все поля заполнены
      if (username && full_name && email && dob && role) {
        // Отправляем ajax-запрос на сервер
        $.ajax({
          url: '/users/'+user_id, // URL для создания пользователя
          type: 'PUT', // Метод запроса
          data: { // Данные, которые отправляем
            name: username,
            full_name: full_name,
            email: email,
            date_of_birth: dob,
            role: role,
            password: password,
            password_confirmation: password_confirm,
            _token: $('meta[name="csrf-token"]').attr('content')
          },
          success: function(response) { // Функция, которая вызывается при успешном ответе
            // Закрываем модальное окно
            $('#editModal').modal('hide');
            // Очищаем форму
            $('#editModal input').val('');
            $('#editModal #select2Basic').val('user');
            // Выводим сообщение об успехе с Sweet Alert
            Swal.fire({
              icon: 'success',
              title: 'Success!',
              text: response.message,
            });
            $('.datatables-basic').DataTable().ajax.reload();
          },
          error: function(error) { // Функция, которая вызывается при ошибке
            // Выводим сообщение об ошибке с Sweet Alert
            Swal.fire({
              icon: 'error',
              title: 'Error!',
              text: error.responseJSON.message,
            });
          }
        });
      } else {
        // Выводим сообщение о том, что нужно заполнить все поля с Sweet Alert
        Swal.fire({
          icon: 'error',
          title: 'Ошибка!',
          text: 'Пожалуйста, заполните все поля',
        });
      }
    });
  </script>
{{--<script src="{{asset('assets/js/app-user-list.js')}}"></script>--}}
@endsection

@section('content')

<div class="row g-4 mb-4">
  <div class="col-sm-6 col-xl-3">
    <div class="card">
      <div class="card-body">
        <div class="d-flex align-items-start justify-content-between">
          <div class="content-left">
            <span>Session</span>
            <div class="d-flex align-items-center my-1">
              <h4 class="mb-0 me-2">21,459</h4>
              <span class="text-success">(+29%)</span>
            </div>
            <span>Total Users</span>
          </div>
          <span class="badge bg-label-primary rounded p-2">
            <i class="ti ti-user ti-sm"></i>
          </span>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-xl-3">
    <div class="card">
      <div class="card-body">
        <div class="d-flex align-items-start justify-content-between">
          <div class="content-left">
            <span>Paid Users</span>
            <div class="d-flex align-items-center my-1">
              <h4 class="mb-0 me-2">4,567</h4>
              <span class="text-success">(+18%)</span>
            </div>
            <span>Last week analytics </span>
          </div>
          <span class="badge bg-label-danger rounded p-2">
            <i class="ti ti-user-plus ti-sm"></i>
          </span>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-xl-3">
    <div class="card">
      <div class="card-body">
        <div class="d-flex align-items-start justify-content-between">
          <div class="content-left">
            <span>Active Users</span>
            <div class="d-flex align-items-center my-1">
              <h4 class="mb-0 me-2">19,860</h4>
              <span class="text-danger">(-14%)</span>
            </div>
            <span>Last week analytics</span>
          </div>
          <span class="badge bg-label-success rounded p-2">
            <i class="ti ti-user-check ti-sm"></i>
          </span>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-xl-3">
    <div class="card">
      <div class="card-body">
        <div class="d-flex align-items-start justify-content-between">
          <div class="content-left">
            <span>Pending Users</span>
            <div class="d-flex align-items-center my-1">
              <h4 class="mb-0 me-2">237</h4>
              <span class="text-success">(+42%)</span>
            </div>
            <span>Last week analytics</span>
          </div>
          <span class="badge bg-label-warning rounded p-2">
            <i class="ti ti-user-exclamation ti-sm"></i>
          </span>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="card-datatable table-responsive pt-0">
  <table class="datatables-basic table">
    <thead>
    <tr>
      <th></th>
      <th></th>
      <th>id</th>
      <th>Name</th>
      <th>Email</th>
      <th>Age</th>
      <th>Status</th>
      <th>Action</th>
    </tr>
    </thead>
  </table>
</div>


<div class="modal fade" id="createModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel1">Create User</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col mb-3">
            <label for="Username" class="form-label">Username</label>
            <input type="text" id="Username" class="form-control" placeholder="Enter Name" name="username">
          </div>
        </div>
        <div class="row">
          <div class="col mb-3">
            <label for="fullname" class="form-label">Full Name</label>
            <input type="text" id="fullname" class="form-control" placeholder="Full Name" name="full_name">
          </div>
        </div>
        <div class="row g-2">
          <div class="col mb-0">
            <label for="emailBasic" class="form-label">Email</label>
            <input type="email" id="emailBasic" class="form-control" placeholder="xxxx@xxx.xx" name="email">
          </div>
          <div class="col mb-0">
            <label for="dobBasic" class="form-label">DOB</label>
            <input type="date" id="dobBasic" class="form-control" name="dob">
          </div>
          <div class="mb-3">
            <label for="select2Basic" class="form-label">Role</label>
            <select id="select2Basic" class="select2 form-select form-select-lg" data-allow-clear="true">
              <option value="user">User</option>
              <option value="driver">Driver</option>
              <option value="admin">Admin</option>
            </select>
          </div>
          <div class="form-password-toggle">
            <label class="form-label" for="password">Password</label>
            <div class="input-group">
              <input type="password" class="form-control" id="password" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="basic-default-password" />
              <span id="basic-default-password" class="input-group-text cursor-pointer"><i class="ti ti-eye-off"></i></span>
            </div>
          </div>
          <div class="form-password-toggle">
            <label class="form-label" for="password_confirm">Password Confirmation</label>
            <div class="input-group">
              <input type="password" class="form-control" id="password_conft" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="basic-default-password" />
              <span id="basic-default-password" class="input-group-text cursor-pointer"><i class="ti ti-eye-off"></i></span>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-label-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Create User</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel1">Edit User</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col mb-3">
            <label for="Username" class="form-label">Username</label>
            <input type="text" id="Username" class="form-control" placeholder="Enter Name" name="username">
          </div>
        </div>
        <div class="row">
          <div class="col mb-3">
            <label for="fullname" class="form-label">Full Name</label>
            <input type="text" id="fullname" class="form-control" placeholder="Full Name" name="full_name">
          </div>
        </div>
        <div class="row g-2">
          <div class="col mb-0">
            <label for="emailBasic" class="form-label">Email</label>
            <input type="email" id="emailBasic" class="form-control" placeholder="xxxx@xxx.xx" name="email">
          </div>
          <div class="col mb-0">
            <label for="dobBasic" class="form-label">DOB</label>
            <input type="date" id="dobBasic" class="form-control" name="dob">
          </div>
          <div class="mb-3">
            <label for="select2Basic" class="form-label">Role</label>
            <select id="select2Basic" class="select2 form-select form-select-lg" data-allow-clear="true">
              <option value="user">User</option>
              <option value="driver">Driver</option>
              <option value="admin">Admin</option>
            </select>
          </div>
          <div class="form-password-toggle">
            <label class="form-label" for="password">Password</label>
            <div class="input-group">
              <input type="password" class="form-control" id="password" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="basic-default-password" />
              <span id="basic-default-password" class="input-group-text cursor-pointer"><i class="ti ti-eye-off"></i></span>
            </div>
          </div>
          <div class="form-password-toggle">
            <label class="form-label" for="password_confirm">Password Confirmation</label>
            <div class="input-group">
              <input type="password" class="form-control" id="password_conft" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="basic-default-password" />
              <span id="basic-default-password" class="input-group-text cursor-pointer"><i class="ti ti-eye-off"></i></span>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-label-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

{{--@section('page-script')--}}

{{--@endsection--}}
@endsection
