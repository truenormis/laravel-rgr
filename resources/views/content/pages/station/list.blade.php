@extends('layouts/layoutMaster')

@section('title', 'Station List - Pages')

@section('vendor-style')
<link rel="stylesheet" href="{{asset('assets/vendor/libs/datatables-bs5/datatables.bootstrap5.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/libs/datatables-responsive-bs5/responsive.bootstrap5.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/libs/datatables-buttons-bs5/buttons.bootstrap5.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/libs/select2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/formvalidation/dist/css/formValidation.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/animate-css/animate.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/libs/sweetalert2/sweetalert2.css')}}" />

@endsection

@section('vendor-script')
<script src="{{asset('assets/vendor/libs/moment/moment.js')}}"></script>
<script src="{{asset('assets/vendor/libs/datatables-bs5/datatables-bootstrap5.js')}}"></script>
<script src="{{asset('assets/vendor/libs/select2/select2.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/FormValidation.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/plugins/Bootstrap5.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/formvalidation/dist/js/plugins/AutoFocus.min.js')}}"></script>
<script src="{{asset('assets/vendor/libs/cleavejs/cleave.js')}}"></script>
<script src="{{asset('assets/vendor/libs/cleavejs/cleave-phone.js')}}"></script>
<script src="{{asset('assets/vendor/libs/sweetalert2/sweetalert2.js')}}"></script>
@endsection

@section('page-script')
  <script>
    $(function() {
      'use strict';

      var dt_basic_table = $('.datatables-basic');

      // DataTable with buttons
      // --------------------------------------------------------------------

      if (dt_basic_table.length) {
        dt_basic = dt_basic_table.DataTable({
          ajax: '{{route('station.list')}}',
          columns: [
            { data: '' },
            { data: 'id' },
            { data: 'id' },
            { data: 'name' },
            { data: '' }
          ],
          columnDefs: [
            {
              // For Responsive
              className: 'control',
              orderable: false,
              searchable: false,
              responsivePriority: 2,
              targets: 0,
              render: function (data, type, full, meta) {
                return '';
              }
            },
            {
              // For Checkboxes
              targets: 1,
              orderable: false,
              searchable: false,
              responsivePriority: 3,
              checkboxes: true,
              render: function () {
                return '<input type="checkbox" class="dt-checkboxes form-check-input">';
              },
              checkboxes: {
                selectAllRender: '<input type="checkbox" class="form-check-input">'
              }
            },
            {
              targets: 2,
              searchable: false,
              visible: false
            },
            {
              // Avatar image/badge, Name and post
              targets: 3,
              responsivePriority: 4,
              render: function (data, type, full, meta) {
                var $name = full['name'];
                // Creates full output for row
                var $row_output =
                  '<div class="d-flex justify-content-start align-items-center user-name">' +
                  '<div class="avatar-wrapper">' +
                  '<div class="avatar me-2">' +
                  '</div>' +
                  '</div>' +
                  '<div class="d-flex flex-column">' +
                  '<span class="emp_name text-truncate">' +
                  $name +
                  '</span>' +
                  '<small class="emp_post text-truncate text-muted">' +
                  '</small>' +
                  '</div>' +
                  '</div>';
                return $row_output;
              }
            },
            {
              responsivePriority: 1,
              targets: 4
            },
            {
              // Label
              targets: -2,
              render: function (data, type, full, meta) {
                var $status_number = full['status'];
                var $status = {
                  1: { title: 'User', class: 'bg-label-primary' },
                  2: { title: 'Driver', class: ' bg-label-success' },
                  3: { title: 'Admin', class: ' bg-label-danger' },

                };
                if (typeof $status[$status_number] === 'undefined') {
                  return data;
                }
                return (
                  '<span class="badge ' + $status[$status_number].class + '">' + $status[$status_number].title + '</span>'
                );
              }
            },
            {
              // Actions
              targets: -1,
              title: 'Actions',
              orderable: false,
              searchable: false,
              render: function (data, type, full, meta) {
                return (
                  '<div class="d-inline-block">' +
                  '<a href="javascript:;" class="btn btn-sm btn-icon dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="text-primary ti ti-dots-vertical"></i></a>' +
                  '<ul class="dropdown-menu dropdown-menu-end m-0">' +
                  '<li><a href="javascript:;" class="dropdown-item">Details</a></li>' +
                  '<li><a href="javascript:;" class="dropdown-item">Archive</a></li>' +
                  '<div class="dropdown-divider"></div>' +
                  '<li><a href="javascript:;" class="dropdown-item text-danger delete-record" data-id="'+full['id']+'">Delete</a></li>' +
                  '</ul>' +
                  '</div>' +
                  '<a href="javascript:;" class="btn btn-sm btn-icon item-edit" data-id="'+full['id']+'" data-bs-toggle="modal" data-bs-target="#editModal"><i class="text-primary ti ti-pencil"></i></a>'
                );
              }
            }
          ],
          order: [[2, 'desc']],
          dom: '<"card-header flex-column flex-md-row"<"head-label text-center"><"dt-action-buttons text-end pt-3 pt-md-0"B>><"row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6 d-flex justify-content-center justify-content-md-end"f>>t<"row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
          displayLength: 7,
          lengthMenu: [7, 10, 25, 50, 75, 100],
          buttons: [
            {
              extend: 'collection',
              className: 'btn btn-label-primary dropdown-toggle me-2',
              text: '<i class="ti ti-file-export me-sm-1"></i> <span class="d-none d-sm-inline-block">Export</span>',
              buttons: [
                {
                  extend: 'print',
                  text: '<i class="ti ti-printer me-1" ></i>Print',
                  className: 'dropdown-item',
                  exportOptions: { columns: [3, 4, 5, 6, 7] }
                },
                {
                  extend: 'csv',
                  text: '<i class="ti ti-file-text me-1" ></i>Csv',
                  className: 'dropdown-item',
                  exportOptions: { columns: [3, 4, 5, 6, 7] }
                },
                {
                  extend: 'pdf',
                  text: '<i class="ti ti-file-description me-1"></i>Pdf',
                  className: 'dropdown-item',
                  exportOptions: { columns: [3, 4, 5, 6, 7] }
                },
                {
                  extend: 'copy',
                  text: '<i class="ti ti-copy me-1" ></i>Copy',
                  className: 'dropdown-item',
                  exportOptions: { columns: [3, 4, 5, 6, 7] }
                }
              ]
            },
            {
              text: '<i class="ti ti-plus me-sm-1"></i> <span class="d-none d-sm-inline-block" data-bs-toggle="modal" data-bs-target="#createModal">Add New Record</span>',
              className: 'create-new btn btn-primary'
            }
          ],
          responsive: {
            details: {
              display: $.fn.dataTable.Responsive.display.modal({
                header: function (row) {
                  var data = row.data();
                  return 'Details of ' + data['full_name'];
                }
              }),
              type: 'column',
              renderer: function (api, rowIdx, columns) {
                var data = $.map(columns, function (col, i) {
                  return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                    ? '<tr data-dt-row="' +
                    col.rowIndex +
                    '" data-dt-column="' +
                    col.columnIndex +
                    '">' +
                    '<td>' +
                    col.title +
                    ':' +
                    '</td> ' +
                    '<td>' +
                    col.data +
                    '</td>' +
                    '</tr>'
                    : '';
                }).join('');

                return data ? $('<table class="table"/><tbody />').append(data) : false;
              }
            }
          }
        });
        $('div.head-label').html('<h5 class="card-title mb-0">DataTable with Buttons</h5>');
      }
    });
    $(document).on('click', '.delete-record', function () {
      var user_id = $(this).data('id');


      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        customClass: {
          confirmButton: 'btn btn-primary me-3',
          cancelButton: 'btn btn-label-secondary'
        },
        buttonsStyling: false
      }).then((result) => {
        if (result.isConfirmed) {
          console.log(user_id);
          $.ajax({
            url: '/station/' + user_id,
            type: 'DELETE', // Метод запроса
            data: { // Данные, которые отправляются на сервер
              _token: $('meta[name="csrf-token"]').attr('content') // Токен для защиты от CSRF атак
            },
            success: function (response) { // Функция, которая вызывается при успешном ответе от сервера
              // После успешного удаления, выведите сообщение
              Swal.fire({
                title: 'Deleted!',
                text: 'The record has been deleted.',
                icon: 'success',
                customClass: {
                  confirmButton: 'btn btn-primary'
                },
                buttonsStyling: false
              });
              // Обновите таблицу с данными пользователей
              $('.datatables-basic').DataTable().ajax.reload();
            },
            error: function (error) { // Функция, которая вызывается при ошибке на сервере
              // Выведите сообщение об ошибке
              Swal.fire({
                title: 'Error!',
                text: error.responseJSON.message,
                icon: 'error',
                customClass: {
                  confirmButton: 'btn btn-primary'
                },
                buttonsStyling: false
              });
            }
          });
        }
      });
    });
    $('#createModal .btn-primary').on('click', function() {
      // Получаем данные из формы
      var name = $('#name').val();

      // Проверяем, что все поля заполнены
      if (name) {
        // Отправляем ajax-запрос на сервер
        $.ajax({
          url: '/station/store', // URL для создания пользователя
          type: 'POST', // Метод запроса
          data: { // Данные, которые отправляем
            name: name,
            _token: $('meta[name="csrf-token"]').attr('content')
          },
          success: function(response) { // Функция, которая вызывается при успешном ответе
            // Закрываем модальное окно
            $('#createModal').modal('hide');
            // Очищаем форму
            $('#createModal input').val('');
            $('#select2Basic').val('user');
            // Выводим сообщение об успехе с Sweet Alert
            Swal.fire({
              icon: 'success',
              title: 'Успех!',
              text: response.message,
            });
            $('.datatables-basic').DataTable().ajax.reload();
          },
          error: function(error) { // Функция, которая вызывается при ошибке
            // Выводим сообщение об ошибке с Sweet Alert
            Swal.fire({
              icon: 'error',
              title: 'Ошибка!',
              text: error.responseJSON.message,
            });
          }
        });
      } else {
        // Выводим сообщение о том, что нужно заполнить все поля с Sweet Alert
        Swal.fire({
          icon: 'error',
          title: 'Ошибка!',
          text: 'Please fill all fields',
        });
      }
    });



    $(document).on('click', '.item-edit', function () {
      // Получаем id пользователя из атрибута data-id
      var station_id = $(this).data('id');
      // Отправляем ajax-запрос на сервер для получения данных пользовател
      $.ajax({
        url: '/station/' + station_id, // URL для получения информации о пользователе
        type: 'GET', // Метод запроса
        success: function(response) { // Функция, которая вызывается при успешном ответе
          // Получаем данные пользователя из ответа
          var station = response.data[0];
          // Заполняем форму редактирования данными пользователя
          $('#editModal #name').val(station.name);
          $('#editModal .btn-primary').data('id',station_id)
        },
        error: function(error) { // Функция, которая вызывается при ошибке
          // Выводим сообщение об ошибке
          alert(error.responseJSON.message);
        }
      });
    });


    $('#editModal .btn-primary').on('click', function() {
      var station_id = $(this).data('id');
      // Получаем данные из формы
      var name = $('#editModal #name').val();


      // Проверяем, что все поля заполнены
      if (name) {
        // Отправляем ajax-запрос на сервер
        $.ajax({
          url: '/station/'+station_id, // URL для создания пользователя
          type: 'PUT', // Метод запроса
          data: { // Данные, которые отправляем
            name: name,
            _token: $('meta[name="csrf-token"]').attr('content')
          },
          success: function(response) { // Функция, которая вызывается при успешном ответе
            // Закрываем модальное окно
            $('#editModal').modal('hide');
            // Очищаем форму
            $('#editModal input').val('');
            $('#editModal #select2Basic').val('user');
            // Выводим сообщение об успехе с Sweet Alert
            Swal.fire({
              icon: 'success',
              title: 'Success!',
              text: response.message,
            });
            $('.datatables-basic').DataTable().ajax.reload();
          },
          error: function(error) { // Функция, которая вызывается при ошибке
            // Выводим сообщение об ошибке с Sweet Alert
            Swal.fire({
              icon: 'error',
              title: 'Error!',
              text: error.responseJSON.message,
            });
          }
        });
      } else {
        // Выводим сообщение о том, что нужно заполнить все поля с Sweet Alert
        Swal.fire({
          icon: 'error',
          title: 'Ошибка!',
          text: 'Пожалуйста, заполните все поля',
        });
      }
    });
  </script>
{{--<script src="{{asset('assets/js/app-user-list.js')}}"></script>--}}
@endsection

@section('content')


<div class="card-datatable table-responsive pt-0">
  <table class="datatables-basic table">
    <thead>
    <tr>
      <th></th>
      <th></th>
      <th>id</th>
      <th>Name</th>
      <th>Action</th>
    </tr>
    </thead>
  </table>
</div>


<div class="modal fade" id="createModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel1">Create Station</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col mb-3">
            <label for="name" class="form-label">Station Name</label>
            <input type="text" id="name" class="form-control" placeholder="Enter Name" name="username">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-label-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Create Station</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel1">Edit Station</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col mb-3">
            <label for="name" class="form-label">Station Name</label>
            <input type="text" id="name" class="form-control" placeholder="Enter Name" name="username">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-label-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Edit Station</button>
      </div>
    </div>
  </div>
</div>

{{--@section('page-script')--}}

{{--@endsection--}}
@endsection
