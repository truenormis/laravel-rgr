<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

return new class extends Migration {
  /**
   * Run the migrations.
   */
  public function up(): void
  {
    Schema::create('trains', function (Blueprint $table) {
      $table->engine = 'InnoDB';
      $table->id();
      $table->string('number')->unique()->default('#' . Str::random(10));
      $table->string('model');
      $table->unsignedBigInteger('user_id')->nullable();
      $table->timestamps();


    });

    Schema::table('trains', function (Blueprint $table) {
      $table->foreign('user_id')
        ->references('id')
        ->on('users')
        ->onDelete('cascade');
    });

  }

  /**
   * Reverse the migrations.
   */
  public function down(): void
  {
    Schema::dropIfExists('trains');
  }
};
