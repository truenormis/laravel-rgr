<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Train>
 */
class TrainFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
  public function definition()
  {
    // массив самых популярных моделей электропоездов в Украине, основанный на результате поиска в Интернете[^1^][1]
    $models = [
      'ER2',
      'ER9',
      'ED9',
      'EP2D',
      'EP20',
      'EC2G',
      'EC2K',
      'EC4K',
      'EC5P',
      'EC6P'
    ];

    return [
      'number' => '#' . str_pad( $this->faker->unique()->numberBetween(1, 999), 3, '0', STR_PAD_LEFT),
      'user_id' => User::where('role', 'driver')->inRandomOrder()->first()->id,
      'model' => $this->faker->randomElement($models),
    ];
  }
}
