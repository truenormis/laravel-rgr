<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Station;
use App\Models\Train;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
  /**
   * Seed the application's database.
   */
  public function run(): void
  {
    User::factory(100)->create();

    User::factory()->create([
      'name' => 'root',
      'role' => 'admin',
      'password' => Hash::make('nfgxtyrb99'),
    ]);

    Train::factory(20)->create();

    Station::factory(20)->create();
  }
}
